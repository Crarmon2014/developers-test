<?php

# challenge 1
function getPiDecimal(){
# your code here
		$pi=pi(); 

   		$get_pi=explode(".",$pi);

   		return $get_pi[1][9];

		//return 5;
}

# challenge 2
function getSumEvens($a=[1,2,3,4,5,6]){
# your code here


		$suma=0;

   		

   		foreach ($a as $value) {


   			if ($value%2==0){

   				$suma+=$value;
   			}


   			
   		}

   		return $suma;
	    //return 12;
}

# challenge 3
function getOrderedVowels($s="just a testing"){
# your code here

	$array = str_split($s); 

    $vocales = array('a', 'e', 'i', 'o', 'u','A','E','I','O','U'); 


    $get_vocales = array_filter($array, function($val) use ($vocales) {return in_array($val, $vocales);});


     return  implode($get_vocales,'');

	//return "uaei";
}

# challenge 4
# obtener el primer id del JSON : https://jsonplaceholder.typicode.com/users
function getFirstId(){

	$url = "https://jsonplaceholder.typicode.com/users";
	$json = file_get_contents($url);
	$obj = json_decode($json);


	foreach ($obj as $key => $value) {

	if($key==0):

	$id=$value->id;

	endif;
	}


	return $id;
}




# DONT EDIT
echo "Running: \n";
echo "challenge 1: ".((getPiDecimal()==5)? "pass" : "fail") . "\n" ;
echo "challenge 2: ".((getSumEvens()==12)? "pass" : "fail" ) . "\n" ;
echo "challenge 3: ".((getOrderedVowels()=="uaei")? "pass" : "fail") . "\n" ;
echo "challenge 4: ".((getFirstId()==1)? "pass" : "fail" ).		 "\n" ;
